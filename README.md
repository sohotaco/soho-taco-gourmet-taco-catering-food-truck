Soho Tacos was founded in 2009 with a single mission: to provide cherished memories to our community. We make our tacos to be part of that experience and make sure that your gathering is the stuff of legend. Call (714) 793-9392 for more information!

Address: 132 E Dyer Rd, Santa Ana, CA 92707, USA

Phone: 714-793-9392
